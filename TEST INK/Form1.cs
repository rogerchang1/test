﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace TEST_INK
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string v_path = @"C:\temp\QVT4333-11F0.dat";
            FileStream fs = new FileStream(v_path, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            string str;
            char[] ch_operatorName;
            char[] ch_deviceName;
            byte[] bt_waferSize;
            byte[] bt_machineNo;
            byte[] bt_indexSizeX;
            byte[] bt_indexSizeY;
            byte[] bt_standardOrientationFlatDirection;
            byte[] bt_finalEditingMachine;
            byte[] bt_mapVersion;
            byte[] bt_mapDataAreaRowSize;
            int mapDataAreaRowSize;
            byte[] bt_mapDataAreaLineSize;
            int mapDataAreaLineSize;
            byte[] bt_MapDataFormGroupManagement;
            char[] ch_waferID;
            byte[] bt_numberOfProbing;
            char[] ch_lotNo;
            byte[] bt_cassetteNo;
            byte[] bt_slotNo;
            byte[] bt;
            int bit1;
            int bit2;
            int bit3;
            int bit4;
            int dieArrayIdx;
            die[] dieArray;
            int pos = 0;
            
            try
            {
                //while ( pos< (int)br.BaseStream.Length)
                //{

                //--------------------------------------------------------------------------
                //Coding Notes: 
                //
                //For 1 byte to int: 
                //byte bt[] = br.ReadBytes(1);
                //int bt_int = (int)bt[0];
                //
                //For 2 bytes to int: 
                //byte bt[] = br.ReadBytes(2);
                //int bt_int = BitConverter.ToInt16(bt, 0);
                //
                //For 4 bytes to int: 
                //byte bt[] = br.ReadBytes(4);
                //int bt_int = BitConverter.ToInt32(bt, 0);
                //--------------------------------------------------------------------------
                
                //Operator Name
                ch_operatorName = br.ReadChars(20);

                //Device Name
                ch_deviceName = br.ReadChars(16);

                //Wafer Size   
                bt_waferSize = br.ReadBytes(2);
                if (BitConverter.IsLittleEndian) Array.Reverse(bt_waferSize);

                //Machine No
                bt_machineNo = br.ReadBytes(2);
                if (BitConverter.IsLittleEndian) Array.Reverse(bt_machineNo);

                //Index size X
                bt_indexSizeX = br.ReadBytes(4);
                if (BitConverter.IsLittleEndian) Array.Reverse(bt_indexSizeX);

                //Index size Y
                bt_indexSizeY = br.ReadBytes(4);
                if (BitConverter.IsLittleEndian) Array.Reverse(bt_indexSizeY);

                //Standard orientation flat direction
                bt_standardOrientationFlatDirection = br.ReadBytes(2);
                if (BitConverter.IsLittleEndian) Array.Reverse(bt_standardOrientationFlatDirection);

                //Final Editing Machine
                bt_finalEditingMachine = br.ReadBytes(1);
                if (BitConverter.IsLittleEndian) Array.Reverse(bt_finalEditingMachine);

                //Map Version
                bt_mapVersion = br.ReadBytes(1);
                if (BitConverter.IsLittleEndian) Array.Reverse(bt_mapVersion);

                //mapDataAreaRowSize
                bt_mapDataAreaRowSize = br.ReadBytes(2);
                if (BitConverter.IsLittleEndian) Array.Reverse(bt_mapDataAreaRowSize);
                mapDataAreaRowSize = BitConverter.ToInt16(bt_mapDataAreaRowSize, 0);

                //mapDataAreaLineSize
                bt_mapDataAreaLineSize = br.ReadBytes(2);
                if (BitConverter.IsLittleEndian) Array.Reverse(bt_mapDataAreaLineSize);
                mapDataAreaLineSize = BitConverter.ToInt16(bt_mapDataAreaLineSize, 0);

                //Map Data Form Group Management
                bt_MapDataFormGroupManagement = br.ReadBytes(4);
                if (BitConverter.IsLittleEndian) Array.Reverse(bt_MapDataFormGroupManagement);

                //Wafer Specific Data  //waferId
                ch_waferID = br.ReadChars(21);

                //Wafer Specific Data //numberOfProbing
                bt_numberOfProbing = br.ReadBytes(1);

                //Wafer Specific Data  //lotNo
                ch_lotNo = br.ReadChars(18);

                //Wafer Specific Data //cassetteNo
                bt_cassetteNo = br.ReadBytes(2);
                if (BitConverter.IsLittleEndian) Array.Reverse(bt_cassetteNo);

                //Wafer Specific Data //slotNo
                bt_slotNo = br.ReadBytes(2);
                if (BitConverter.IsLittleEndian) Array.Reverse(bt_slotNo);

                /*br.ReadBytes(114);

                //Test Die Inforamtion Address
                bt = br.ReadBytes(4);
                if (BitConverter.IsLittleEndian) Array.Reverse(bt);
                Console.WriteLine(BitConverter.ToString(bt, 0));
                Console.WriteLine(BitConverter.ToInt32(bt, 0));

                //numberOfLineCategoryData
                bt = br.ReadBytes(4);
                if (BitConverter.IsLittleEndian) Array.Reverse(bt);
                Console.WriteLine(BitConverter.ToInt32(bt, 0));

                //linCategoryAddress
                bt = br.ReadBytes(4);
                if (BitConverter.IsLittleEndian) Array.Reverse(bt);
                Console.WriteLine(BitConverter.ToInt32(bt, 0));

                //# Extended Map Inforamtion

                //mapFileConfiguration
                bt = br.ReadBytes(2);
                if (BitConverter.IsLittleEndian) Array.Reverse(bt);
                Console.WriteLine(BitConverter.ToInt16(bt, 0));

                //maxMultiSite
                bt = br.ReadBytes(2);
                if (BitConverter.IsLittleEndian) Array.Reverse(bt);
                Console.WriteLine(BitConverter.ToInt16(bt, 0));

                //maxCategories
                bt = br.ReadBytes(2);
                if (BitConverter.IsLittleEndian) Array.Reverse(bt);
                Console.WriteLine(BitConverter.ToInt16(bt, 0));

                //reserved
                bt = br.ReadBytes(2);
                if (BitConverter.IsLittleEndian) Array.Reverse(bt);
                Console.WriteLine(BitConverter.ToInt16(bt, 0));*/




                //READ DIE--------------------------------------------------------

                //Skip the header information for testing
                //br.ReadBytes(236);
                pos = 236;
                br.BaseStream.Seek(pos, SeekOrigin.Begin);
                dieArrayIdx = 0;

                //temporarily initialize the dieArray with 5000 spaces for testing
                int dieArrayLength = mapDataAreaRowSize * mapDataAreaLineSize;
                //int dieArrayLength = 5000;
                dieArray = new die[dieArrayLength];

                while (br.BaseStream.Position != br.BaseStream.Length && dieArrayIdx < dieArray.Length)
                {
                    //Get the next 6 bytes (1 word = 2 bytes)
                    bt = br.ReadBytes(6);
                    die d = new die(bt);
                    dieArray[dieArrayIdx] = d;
                    dieArrayIdx++;
                }

                string printLine = "";
                for(int i = 0; i < dieArray.Length; i++)
                {
                    if (i % mapDataAreaRowSize == 0)
                    {
                        Console.WriteLine(printLine);
                        printLine = "";
                    }
                    else
                    {
                        printLine = printLine + dieArray[i].txt;
                    }
                    
                }
                Console.WriteLine(printLine);
                Console.WriteLine("debug break");
                //test

            }
            finally
            {
                Console.Read();
            }
        }
}
}
