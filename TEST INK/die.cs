﻿using System;

namespace TEST_INK
{
    internal class die
    {
        public int dieTestResult;
        public int dieProperty;
        public int dummyData;
        public int bin;
        public byte[] bt;
        public char txt;

        //bin number is index, element is the corresponding txt value. there is no bin0, so at 0 index, I put 0.
        private char[] binDictionary = {'0','1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', '<', 'n', 'o', 'p', 'q', 'r', ':', 't', 'u', 'v', 'w', 'x', 'y', '=', '!', '@' };

        public die()
        {
        }

        public die(byte[] a)
        {
            bt = a;
            dieTestResult = bt[0] >> 6; //Die Test Result
            dieProperty = bt[2] >> 6; //Skip/Marked value
            dummyData = 1 & (bt[2] >> 1); //Dummy Data
            bin = 1 + (bt[5] & 63); //Check bin number. GREATEK .dat bin is offset by 1.
            calculateTxt();
        }

        public void calculateTxt()
        {
            if(dummyData == 1)
            {
                txt = '.';
            }else if(dummyData == 0 && dieProperty == 0)
            {
                txt = '~';
            }
            else if (dummyData == 0 && dieProperty == 2)
            {
                txt = '#';
            }
            else
            {
                txt = binDictionary[bin];
            }
        }

        public void set_byte(byte[] a)
        {
            bt = a;
            dieTestResult = bt[0] >> 6; //Die Test Result
            dieProperty = bt[2] >> 6; //Skip/Marked value
            dummyData = 1 & (bt[2] >> 1); //Dummy Data
            bin = 1 + (bt[5] & 63); //Check bin number. GREATEK .dat bin is offset by 1.
            calculateTxt();
        }

        //Set die test result will also change the byte and txt value
        public void set_dieTestResult(int a)
        {
            //dieTestResult should be 2 bits
            if(a >= 0 && a < 4)
            {
                dieTestResult = a;
                int bt_int = (int)bt[0] & 63; // & original byte with 00111111 to see what is the right 6 bits and store it to bt_int
                int intToByte = (dieTestResult << 6) | bt_int;
                bt[0] = BitConverter.GetBytes(intToByte)[0];
                calculateTxt();
            }
        }

        //Set die property will also change the byte and txt value
        public void set_dieProperty(int a)
        {
            
            //dieProperty should be 2 bits
            if (a >= 0 && a < 4)
            {
                dieProperty = a;
                int bt_int = (int)bt[2] & 63; // & original byte with 00111111 to see what is the right 6 bits and store it to bt_int
                int intToByte = (dieProperty << 6) | bt_int;
                bt[0] = BitConverter.GetBytes(intToByte)[0];
                calculateTxt();
            }
        }

        //Set dummy data will also change the byte and txt value
        public void set_dummyData(int a)
        {
            //dummyData should be 1 bit
            if (a >= 0 && a <= 1)
            {
                //If bit is different, just need to flip the bit, otherwise no change is needed
                if(dummyData != a)
                {
                    dummyData = a;
                    int bt_int = (int)bt[2];
                    int intToByte = bt_int & ~(1 << 1);
                    bt[2] = BitConverter.GetBytes(intToByte)[0];
                    calculateTxt();
                }
            }
        }

        //Set bin will also change the byte value and txt value
        public void set_bin(int a)
        {
            //bin should be between 0 to 63 (00111111)
            if (a >= 0 && a <= 63)
            {
                bin = a;
                int binToByte = bin - 1; //offset for GREATEK
                int bt_int = (int)bt[5] & 128; // & original byte with 10000000 to see what is the most significant bit and store it to bt_int
                int binResult = binToByte | bt_int;// | the original bit with the new bin number
                bt[5] = BitConverter.GetBytes(binResult)[0];
                calculateTxt();
            }
        }

        public int get_dieTestResult()
        {
            return dieTestResult;
        }

        public int get_dieProperty()
        {
            return dieProperty;
        }

        public int get_dummyData()
        {
            return dummyData;
        }

        public int get_bin()
        {
            return bin;
        }

        public byte[] get_byte()
        {
            return bt;
        }

        public char get_txt()
        {
            return txt;
        }
    }
}